<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 24/04/2016
 * Time: 16:10
 */

namespace Model\Manager;


use AMFram\Manager;
use Entity\PostEntity;

abstract class dashboardManager extends Manager
{
    abstract public function showIndexProgram($id_user);
    abstract public function showProfile($id_user);
    abstract public function addPost(PostEntity $post);
    abstract public function bindUserToPost($id_user,$id_post);
    abstract public function showPost();
    abstract public function getLastInsertId();
    
}