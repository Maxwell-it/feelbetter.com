<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 24/04/2016
 * Time: 16:11
 */

namespace Model\Manager;


use Entity\PostEntity;

class dashboardManagerPDO extends dashboardManager
{
    public function showIndexProgram($id_user)
    {
        $sql = "SELECT id_prog_creer, programmes.nom_prog, programmes.description FROM programmes, prog_creer, user 
                WHERE programmes.id_prog = prog_creer.id_prog AND user.id_user = prog_creer.id_user AND user.id_user = :id LIMIT 3";
        $query = $this->dao->prepare($sql);
        $query->bindValue("id", $id_user, \PDO::PARAM_INT);
        $query->execute();
        $query->setFetchMode(\PDO::FETCH_ASSOC);
        return $query->fetchAll();
    }

    public function showProfile($id_user)
    {
        $sql = "SELECT username, avatar, exp, bio FROM user WHERE id_user = :id";
        $request = $this->dao->prepare($sql);
        $request->bindValue("id",$id_user, \PDO::PARAM_INT);
        $request->execute();
        return $request->fetch();
    }

    public function addPost(PostEntity $post)
    {
        $sql = "INSERT INTO post SET id_post='', contenu_post= :content, post_date= NOW(), picto = :picto, video= :video";
        $query = $this->dao->prepare($sql);
        $query->bindValue('content', $post->content(), \PDO::PARAM_STR);
        $query->bindValue('picto', $post->picture(), \PDO::PARAM_STR);
        $query->bindValue('video', $post->video(), \PDO::PARAM_STR);
        $query->execute();
    }

    public function showPost()
    {

    }

    public function bindUserToPost($id_user, $id_post)
    {
        $sql = "INSERT INTO post_user SET id_post_user='',id_user = :id_user,id_post=:id_post";
        $query= $this->dao->prepare($sql);
        $query->bindValue("id_user",$id_user);
        $query->bindValue("id_post",$id_post);
        $query->execute();
    }

    public function getLastInsertId()
    {
        return $this->dao->lastInsertId();
    }
}