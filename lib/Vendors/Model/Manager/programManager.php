<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 20/06/2016
 * Time: 09:14
 */

namespace Model\Manager;


use AMFram\Manager;

abstract class programManager extends Manager
{
    abstract public function getListProgram($id_user);
    abstract public function getListExercise();
}