<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 06/08/2016
 * Time: 20:53
 */

namespace Model\Manager;


use AMFram\Manager;
use Entity\userEntity;

abstract class profilManager extends Manager
{
    abstract public function updateProfile(userEntity $user);
    abstract public function showProfile($id_user);
}