<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 17/05/2016
 * Time: 20:42
 */

namespace Model\Manager;


use AMFram\Manager;
use Entity\userEntity;

class registerManagerPDO extends registerManager
{

    public function save(userEntity $user)
    {
        $sql = "INSERT INTO user 
                SET id_user='NULL', username= :username, password= :password, email= :email, 
                avatar= 'user.png', register_date=NOW(), exp='10',
                bio ='Quel objectifs souhaitez vous atteindre avec les warriors ?' ";
        $request = $this->dao->prepare($sql);
        $request->bindValue('username',$user->getPseudo());
        $request->bindValue('password',$user->getPassword());
        $request->bindValue('email',$user->getEmail());
        $request->execute();
    }
    
    public function getLastInsertId()
    {
        return $this->dao->lastInsertId();
    }
}