<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 03/06/2016
 * Time: 20:10
 */

namespace Model\Manager;


use AMFram\Manager;
use Entity\Exercice;
use Entity\Programs;

abstract class addProgramManager extends Manager
{
    abstract public function addProgram(Programs $program);
        
    abstract public function exoMuscSelect();

    abstract public function exoCardioSelect();
    
    abstract public function addExercice(Exercice $exercice);
    
    abstract public function getLastInsertId();

    abstract  public function bindExoToProg($idProgram, $idExo);
    
    abstract public function bindUserToProg($id_user, $id_program);
        
}