<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 20/06/2016
 * Time: 09:17
 */

namespace Model\Manager;


class programManagerPDO extends programManager
{

    public function getListProgram($id_user)
    {
        $sql = "SELECT id_prog_creer, programmes.nom_prog, programmes.description  
                FROM programmes, prog_creer, user 
                WHERE programmes.id_prog = prog_creer.id_prog
                AND user.id_user = prog_creer.id_user
                AND user.id_user = :id
                LIMIT 10";
        $query = $this->dao->prepare($sql);
        $query->bindValue("id", $id_user, \PDO::PARAM_INT);
        $query->execute();
        $query->setFetchMode(\PDO::FETCH_ASSOC);
        return $query->fetchAll();
    }

    public function getListExercise()
    {
        $sql = "SELECT libelle, nb_serie, nb_repeat, nom_prog, exercice.description, picto_name 
                FROM exercice, programmes, exo_prog, list_exercice, prog_creer 
                WHERE exo_prog.id_prog = programmes.id_prog 
                AND exo_prog.id_exo = exercice.id_exo 
                AND exercice.id_list_exo = list_exercice.id_list_exo
                AND programmes.id_prog = prog_creer.id_prog";
        $query = $this->dao->query($sql);
        $query->setFetchMode(\PDO::FETCH_ASSOC);
        return $query->fetchAll();
    }
}