<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 17/05/2016
 * Time: 20:40
 */

namespace Model\Manager;


use AMFram\Manager;
use Entity\userEntity;

abstract class registerManager extends Manager
{
    abstract public function save(userEntity $user);
    abstract public function getLastInsertId();
}