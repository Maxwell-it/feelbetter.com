<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 06/08/2016
 * Time: 20:54
 */

namespace Model\Manager;


use Entity\userEntity;

class profilManagerPDO extends profilManager
{
    public function updateProfile(userEntity $user)
    {
        $sql = "UPDATE user SET avatar = :avatar, bio = :bio WHERE id_user = :id_user";
        $request = $this->dao->prepare($sql);
        $request->bindValue("id_user",$user->id());
        $request->bindValue("avatar", $user->getAvatar());
        $request->bindValue("bio", $user->getBio());
        $request->execute();
    }

    public function showProfile($id_user)
    {
        $sql = "SELECT username, avatar, exp, bio FROM user WHERE id_user = :id";
        $request = $this->dao->prepare($sql);
        $request->bindValue("id",$id_user, \PDO::PARAM_INT);
        $request->execute();
        return $request->fetch();
    }
}