<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 25/04/2016
 * Time: 23:28
 */

namespace Model\Manager;

use Entity\userEntity;
use AMFram\Manager;

class connexionManagerPDO extends connexionManager
{
    public function getUser(userEntity $user)
    {
        $sql = "SELECT id_user, username, password, email FROM user WHERE username = :pseudo";
        $query = $this->dao->prepare($sql);
        $query->bindValue("pseudo", $user->getPseudo(), \PDO::PARAM_STR);
        $query->execute();
        $result = $query->fetch();

        if($result["password"] == $user->getPassword())
        {
            return $result;
        }
        else 
        {
            return false;
        }

    }
}