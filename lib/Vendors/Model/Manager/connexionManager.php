<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 25/04/2016
 * Time: 23:27
 */

namespace Model\Manager;

use Entity\userEntity;
use AMFram\Manager;

abstract class connexionManager extends Manager
{    
    abstract  public function getUser(userEntity $user);
}