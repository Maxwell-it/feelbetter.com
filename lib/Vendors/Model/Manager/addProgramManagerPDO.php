<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 03/06/2016
 * Time: 20:12
 */

namespace Model\Manager;

use Entity\Exercice;
use Entity\Programs;

class addProgramManagerPDO extends addProgramManager
{
    public function addProgram(Programs $program)
    {
        $sql = "INSERT INTO programmes SET id_prog='', nom_prog= :name_program, description= :description, prog_date=NOW()";
        $query= $this->dao->prepare($sql);
        $query->bindValue('name_program',$program->getName_program());
        $query->bindValue('description',$program->getDescription());
        $query->execute();
    }

    
    public function exoMuscSelect()
    {
        $sql = "SELECT * FROM list_exercice WHERE type='musculation'";
        $query = $this->dao->query($sql);
        $query->setFetchMode(\PDO::FETCH_ASSOC);
        $exercice = $query->fetchAll();
        
        return $exercice;
    }

    public function exoCardioSelect()
    {
        $sql = "SELECT * FROM list_exercice WHERE type='cardio'";
        $query = $this->dao->query($sql);
        $query->setFetchMode(\PDO::FETCH_ASSOC);
        $exercice = $query->fetchAll();
        
        return $exercice;
    }


    public function addExercice(Exercice $exercice)
    {
        $sql = "INSERT INTO exercice SET id_exo = '', id_list_exo = :idlistexo,
        description= :description, nb_serie= :serie, nb_repeat= :rep, 
        temps_repos= :repos, picto_name=:picto, vid_name='video'";

        $query = $this->dao->prepare($sql);
        $query->bindValue('idlistexo',$exercice->getIdlistexo());
        $query->bindValue('description', $exercice->getDescription());
        $query->bindValue('serie', $exercice->getSerie());
        $query->bindValue('rep', $exercice->getRep());
        $query->bindValue('repos', $exercice->getRepos());
        $query->bindValue('picto',$exercice->getExoPic());
        $query->execute();
    }

    public function getLastInsertId()
    {
        return $this->dao->lastInsertId();
    }

    public function bindExoToProg($idProgram,$idExo)
    {
        $sql = "INSERT INTO exo_prog SET id_exo_prog='', id_prog=:idProgram, id_exo= :idExo";
        $query = $this->dao->prepare($sql);
        $query->bindValue("idProgram",$idProgram);
        $query->bindValue("idExo",$idExo);
        $query->execute();
    }

    public function bindUserToProg($id_user, $id_program)
    {
        $sql = "INSERT INTO prog_creer SET id_prog_creer = '', id_user= :id_user, id_prog = :id_program";
        $query = $this->dao->prepare($sql);
        $query->bindValue("id_user",$id_user);
        $query->bindValue("id_program", $id_program);
        $query->execute();
    }
}