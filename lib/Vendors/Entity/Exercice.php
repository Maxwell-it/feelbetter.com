<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 15/06/2016
 * Time: 20:35
 */

namespace Entity;


use AMFram\Entity;

class Exercice extends Entity
{
    protected $id;
    protected $idlistexo;
    protected $description;
    protected $serie;
    protected $rep;
    protected $repos;
    protected $picto;
    protected $video;

    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdlistexo()
    {
        return $this->idlistexo;
    }
    public function setIdlistexo($idlistexo)
    {
        $this->idlistexo = $idlistexo;
    }

    public function getDescription()
    {
        return $this->description;
    }
    public function setDescription($desc)
    {
        $this->description = $desc;
    }

    public function getSerie()
    {
        return $this->serie;
    }
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    public function getRep()
    {
        return $this->rep;
    }
    public function setRep($rep)
    {
        $this->rep = $rep;
    }
    
    public function getRepos()
    {
        return $this->repos;
    }
    public function setRepos($repos)
    {
        $this->repos = $repos;
    }
    
    public function getExoPic()
    {
        return $this->picto;
    }
    
    public function setExoPic($picto)
    {
        $this->picto = $picto;
    }


}