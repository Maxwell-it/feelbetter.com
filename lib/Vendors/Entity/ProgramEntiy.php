<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 07/04/2016
 * Time: 00:29
 */

namespace Entity;


class ProgramEntity extends Entity
{
    protected $name;
    protected $type;
    protected $description;
    protected $date;
    
    const NAME_INVALID = 1;
    const TYPE_INVALID = 2;
    const DESCRIPTION_INVALID = 3;
    
    public function isValid(){
        return !(empty($this->name) || empty($this->type) || empty($this->description));
    }
    
    /********** SETTER **********/
    public function setName($name){
        if(!is_string($name) || empty($name))
        {
            $this->erreur [] = self::NAME_INVALID;
        }
        $this->auteur = $name;
    }
    
    public function setType($type){
        if(!is_string($type) || empty($type))
        {
            $this->erreur [] = self::TYPE_INVALID;
        }
        $this->type = $type;
    }
    
    public function setDescription($desc)
    {
        if(!is_string($desc) || empty($desc))
        {
            $this->erreur [] = self::DESCRIPTION_INVALID;
        }
        $this->description = $desc;
    }
    
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }
    
    /******** GETTERS *******/
    
    public function name()
    {
        return $this->name;
    }
    
    public function type()
    {
        return $this->type;
    }
    
    public function desc()
    {
        return $this->description;
    }
    
    public function date()
    {
        return $this->date;
    }
    
}