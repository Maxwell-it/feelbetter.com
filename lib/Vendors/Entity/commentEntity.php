<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 10/04/2016
 * Time: 17:19
 */

namespace Entity;


use AMFram\Entity;

class commentEntity extends Entity
{
    protected $id,$user, $post, $content, $date;
    const USER_INVLID = 1;
    const CONTENT_INVALID = 2;

    public function is_valid()
    {
        return !(empty($this->user) || empty($this->content) || empty($this->post));
    }

    public function setUser($user)
    {
        $this->user = (int) $user;
    }
    public function setPost($post)
    {
        $this->post = (int) $post;
    }
    
    public function setContent($content)
    {
        if (!is_string($content) || empty($content))
        {
            return $this->erreur [] = self::CONTENT_INVALID;
        }
        $this->content = $content;
    }
    
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }
    
    public function user()
    {
        return $this->user;
    }
    
    public function post()
    {
        return $this->post;
    }
    public function content()
    {
        return $this->content;
    }
    public function date()
    {
        return $this->date;
    }
}