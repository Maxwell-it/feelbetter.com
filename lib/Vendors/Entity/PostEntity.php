<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 08/04/2016
 * Time: 16:35
 */

namespace Entity;


use AMFram\Entity;

class PostEntity extends Entity
{
    protected $id;
    protected $content;
    protected $picture;
    protected $video;
    protected $date;
    
    const INVALID_CONTENT = 1;
    const INVALID_DATE = 2;
    const INVALID_PICTURE= 3;
    const INVALID_VIDEO = 4;

    public function is_valid()
    {
        return (!(empty($this->content)) || !(empty($this->picture) || !(empty($this->video))));
    }
    
    
    /******************* setter ***************************/
    
    public function setContent($content)
    {
        if(!is_string($content) || empty($content))
        {
            $this->erreur [] = self::INVALID_CONTENT;
        }
        $this->content = $content;
    }

    public function setPicture($picture)
    {
        if(!is_string($picture) || empty($picture))
        {
            $this->erreur [] = self::INVALID_PICTURE;
        }
        $this->picture = $picture;
    } 
    
    public function setVideo($video)
    {
        if(!is_string($video) || empty($video))
        {
            $this->erreur [] = self::INVALID_VIDEO;
        }
        $this->video = $video;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }
    
    /************************ getter ***********************/
    
    public function content()
    {
        return $this->content;
    }

    public function picture()
    {
        return $this->picture;
    }
    
    public function video()
    {
        return$this->video;
    }
    
    public function date()
    {
        return $this->date;
    }
    

}