<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 03/06/2016
 * Time: 21:58
 */

namespace Entity;


use AMFram\Entity;

class Programs extends Entity
{
    protected $id;
    protected $name_program;
    protected $description;

    const INVALID_NAME_PROGRAM = 1;
    const INVALID_DESCRIPTION = 2;

    public function is_valid()
    {
        return(!(empty($this->name_program )));
    }
    
    public function id($id)
    {
        $this->id=$id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setName_program($name_program)
    {
        $this->name_program=$name_program;
    }
    
    public function  setDescription($description)
    {
        $this->description = $description;
    }

    public function getName_program()
    {
        return $this->name_program;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
}