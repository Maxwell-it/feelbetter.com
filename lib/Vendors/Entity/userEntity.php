<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 25/04/2016
 * Time: 23:05
 */

namespace Entity;

use AMFram\Entity;

class userEntity extends Entity
{
    protected $id;
    protected $pseudo;
    protected $password;
    protected $email;
    protected $avatar;
    protected $date;
    protected $exp;
    protected $session;
    protected $bio;

    const INVALID_PSEUDO = 1;
    const INVALID_PASSWORD = 2;
    const  INVALID_EMAIL = 3;
    const  INVALID_SESSION = 4;

    public function is_valid()
    {
        return (!(empty($this->pseudo) || empty($this->password) || empty($this->email)));
    }

    /********************** setter *******************/
    

    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setSession($session)
    {
        $this->session = $session;
    }
    
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
    
    public function setBio($bio)
    {
        $this->bio = $bio;
    }
    
    /**************** getter ****************/

    public function getPseudo()
    {
        return $this->pseudo;
    }
    
    public function getPassword()
    {
        return $this->password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }
    public function getDate()
    {
       return $this->date;
    }
    
    public function getExp()
    {
       return $this->exp;
    }
    
    public function getBio()
    {
        return $this->bio;
    }
}