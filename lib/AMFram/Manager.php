<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 05/04/2016
 * Time: 21:25
 */

namespace AMFram;


abstract class Manager
{
    protected $dao;

    public function __construct($dao)
    {
        $this->dao = $dao;
    }
}