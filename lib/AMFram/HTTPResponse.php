<?php 
namespace AMFram;

class HTTPResponse extends ApplicationComponent
{
	protected $page;
	
	public function addHeader($header)
	{
		header($header);
	}
	
	// function de redirection 
	public function redirection($location)
	{
		header('Location:'.$location);
		exit;
	}
	
	//function qui retourne une erreur 404
	public function redirect404()
	{
		$this->page = new Page($this->app);
		$this->page->setContentFile(__DIR__.'/../../Error/404.html');
        $this->addHeader('HTTP/1.0 404 Not Found');
        $this->send();
	}
	
	//envoie de la page demandé
	public function send()
	{
		exit($this->page->getGeneratedPage());
	}
	
	//setter de la page
	public function setPage(Page $page)
	{
		$this->page = $page; 
	}
	
	// Changement par rapport à la fonction setcookie() : le dernier argument est par défaut à true
	public function setCookie($name, $value = '', $expire = 0, $path = null, $domain = null, $secure = false, $httpOnly = true)
	{
		setcookie($name, $value, $expire, $path, $domain, $secure, $httpOnly);
	}
}
?>