<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 05/04/2016
 * Time: 21:30
 */

namespace AMFram;


class PDOFactory
{
    public static function getMysqlConnexion()
    {
        $db = new \PDO('mysql:host=localhost;dbname=feelbetter','root','',array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $db ->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $db;
    }
}