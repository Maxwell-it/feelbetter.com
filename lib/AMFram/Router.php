<?php 
namespace AMFram;

class Router 
{
	protected $routes = [];
	
	const NO_ROUTE = 1;
	
	public function addRoute(Route $route)
	{
		if (!in_array($route, $this->routes))
		{
			$this->routes[] = $route;
		}
	}
	
	public function getRoute($url)
	{
		foreach ($this->routes as $route)
		{
			// verifie si la route correspond à l'url
			if (($varsValues = $route->match($url)) !== false)
			{
				// si la route a des variables
				if ($route->hasVars())
				{
					$varsNames = $route->varsNames();
					$listVars = [];
					
					//on crée un nouveau tableau clé val
					// cle = nom de la var, val = val de la var
					foreach ($varsValues as $key => $match)
					{
						// la première valeur contient la chaine capturé 
						if($key !== 0)
						{
							$listVars[$varsNames[$key - 1]] = $match; 
						}
					}
					$route->setVars($listVars);
				}
				
				return $route;
			}
		}
		throw new \RuntimeException('Aucun route ne correspond a l\'URL ', self::NO_ROUTE);
	}
}
?>