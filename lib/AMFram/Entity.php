<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 05/04/2016
 * Time: 21:37
 */

namespace AMFram;


abstract class Entity implements \ArrayAccess
{
    protected $erreurs = [];
    protected $id;

    public function __construct($data)
    {
        if (!empty($data)){
            $this->hydrate($data);
        }
    }

    public function isNew()
    {
        return empty($this->id);
    }

    public function erreurs()
    {
        return $this->erreurs();
    }

    public function id()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
    }

    public function hydrate(array $data)
    {
        foreach ($data as $attribut => $valeur){
            $method = 'set'.ucfirst($attribut);

            if (is_callable([$this,$method])){
                $this->$method($valeur);
            }
        }
    }

    public function offsetGet($var)
    {
        if (isset($this->$var) && is_callable([$this, $var])){
            return $this->$var();
        }
    }

    public function offsetSet($var, $val)
    {
        $method2 = 'set'.ucfirst($var);
        if (isset($this->$var) && is_callable([$this, $method2]))
        {
            $this->$method2($val);
        }
    }

    public function offsetExists($var)
    {
        return isset($this->$var) && is_callable([$this,$var]);
    }

    public function offsetUnset($var)
    {
        throw new \Exception('Implossible de supprimer une quelconque valeur');
    }

}