<?php
	namespace AMFram;
    
	class HTTPRequest extends ApplicationComponent
	{
		// definiton de la fonction cookie
		public function cookieData($data){
			return isset($_COOKIE[$data]) ? $_COOKIE[$data] : null;
		}
		
		// verifie si cookie existe
		public function cookieExists($data){
			return isset($_COOKIE[$data]);
		}
		
		// on recupere la donnée par la method GET 
		public function getData($data){
			return isset($_GET[$data]) ? $_GET[$data] : null;
		}
		
		// si donnée GET  existe 
		public function getExists($data){
			return isset($_GET[$data]);
		}
		
		public function method()
		{
			return $_SERVER['REQUEST_METHOD'];
		}
		
		// recuperation par la method POST 
		public function postData($data){
			return isset($_POST[$data]) ? $_POST[$data] : null;
		}
		
		public function postExists($data){
			return isset($_POST[$data]);
		}
		
		public function requestURI(){
			return $_SERVER['REQUEST_URI'];
		}
		
	}
?>