<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 06/04/2016
 * Time: 00:19
 */

namespace AMFram;

session_start();

class User
{
    public function getAttribute($attr)
    {
        return isset($_SESSION[$attr])? $_SESSION[$attr] : null;
    }

    public function getFlash()
    {
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);

        return $flash;
    }

    public  function hasFlash()
    {
        return isset($_SESSION['flash']);
    }

    public function isAuthenticated()
    {
        return isset($_SESSION['auth']) && $_SESSION['auth'] === true;
    }

    public function setAttribute($attr, $val)
    {
        $_SESSION{$attr} = $val;
    }

    public function setAuthenticated($authentificated = true)
    {
        if (!is_bool($authentificated))
        {
            throw new \InvalidArgumentException('La veleur spécifié à la methode User::setAuthentificated() doit être un boolean');
        }
        $_SESSION['auth'] = $authentificated;
    }

    public function setFlash($val)
    {
        $_SESSION['flash'] = $val;
    }
}