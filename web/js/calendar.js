$(document).ready(function () {
    $('#calendar').fullCalendar({
        height: 408,
        defaultView: 'month',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        }
    })
});