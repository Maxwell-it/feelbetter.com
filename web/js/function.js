/**
 * Created by maxwell on 07/06/2016.
 */

$(document).ready(function () {

    // select initialisation
    $('select').material_select();

    //modal trigger initalisation
    $('.modal-trigger').leanModal();

    // initialisation parallax
    $(".parallax").parallax();
    
    //initialisation character 
    $('textarea#bio').characterCounter();

    /*************** gestion error nom_programme ***********/
    $('.program_name_submit').click(function(event){
        if($("#name_program").val()=="")
        {
            $("#name_program").addClass("invalid");
            $(".error").css("display","block");
            return false;
        }
        else
        {
            Materialize.toast('I am a toast', 3000,'',function(){});
            event.preventDefault();
            $("#modal-prog").openModal(
                {
                    complete: function ()
                    {
                        $(".form").submit();
                    }
                }
            )
        }
    });

});