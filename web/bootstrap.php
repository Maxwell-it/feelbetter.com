<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 06/04/2016
 * Time: 16:01
 */


const DEFAULT_APP = 'frontend';


// si l'application  n'est pas valide on chargera l'application  
// par default qui va générer une erreur 404

if (!isset($_GET['app']) || !file_exists(__DIR__.'../app/'.$_GET['app']))
{
    $_GET['app'] = DEFAULT_APP;
}

// inclure la classe autoload 
$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->add('app\\frontend\\', __DIR__);



$appClass = 'app\\'.$_GET['app'].'\\'.$_GET['app'].'Application';

$app = new $appClass;
$app->run();




