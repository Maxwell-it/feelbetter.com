<?php
namespace app\frontend\Module\deconnexion;
use AMFram\BackController;
use AMFram\HTTPRequest;

/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 30/07/2016
 * Time: 01:24
 */
class deconnexionController extends BackController
{
    public function executeDeconnexion(HTTPRequest $request)
    {
        $this->app->user()->setAuthenticated(false);
        $this->app->httpResponse()->redirection(".");
    }
}