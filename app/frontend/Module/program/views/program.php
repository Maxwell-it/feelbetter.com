<div class="row">
    <div class="col l2 m2 s6 sidebar">
        <?php include ('/../../../Templates/sidebar.php');?>
    </div>

    <div class="col l10 m10 s10">
        <div class="middle">
            <nav>
                <div class="nav-wrapper">
                    <div class="col s12">
                        <a href="#!" class="breadcrumb"></a>
                        <a href="index" class="breadcrumb">dashboard</a>
                        <a href="program" class="breadcrumb">programmes</a>
                    </div>
                </div>
            </nav>

            <div class="list-program">
                <div class="row">
                    <div class="collection-content col l3 m3 s3">
                        <ul class="collection">
                            <li class="collection-item active"> Mes programmes </li>
                            <?php foreach ($listProgram as $program) {?>
                            <li class="collection-item avatar">
                                <i class="material-icons circle grey">assignment</i>
                                <span class="title"><b><?= $program['nom_prog'] ?> </b></span>
                                <p class="description"> <?= $program["description"] ?></p>
                            </li>
                            <?php }?>
                            <!--<li class="collection-item avatar">
                                <i class="material-icons circle grey">assignment</i>
                                <span class="title"><b>Nom du programme</b></span>
                                <p> Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>
                            </li>
                            <li class="collection-item avatar">
                                <i class="material-icons circle grey">assignment</i>
                                <span class="title"><b>Nom du programme</b></span>
                                <p> Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>

                            </li>
                            <li class="collection-item avatar">
                                <i class="material-icons circle grey">assignment</i>
                                <span class="title"><b>Nom du programme</b></span>
                                <p> Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>

                            </li>
                            <li class="collection-item avatar">
                                <i class="material-icons circle grey">assignment</i>
                                <span class="title"><b>Nom du programme</b></span>
                                <p> Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>
                            </li>
                            <li class="collection-item avatar">
                                <i class="material-icons circle grey">assignment</i>
                                <span class="title"><b>Nom du programme</b></span>
                                <p> Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>
                            </li>-->
                        </ul>
                    </div>
                    <div class="col l9 m9 s9">
                        <div class="program-content">
                            <div class="row">
                                <div class="col l1 m2 s2">
                                    <a href="#" class="btn-floating waves-effect grey">
                                        <i class="material-icons">chevron_left</i>
                                    </a>
                                </div>
                                <div class="col l10 m8 s8">
                                    <h6>Lundi</h6>
                                </div>
                                <div class="col l1 m2 s2">
                                    <a href="#" class="btn-floating waves-effect grey">
                                        <i class="material-icons">chevron_right</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a class="btn-floating red" style="position: fixed; bottom: 20px;right: 40px;">
                            <i class="large material-icons">add</i>
                        </a>
                        <div class="training-content">
                            <h5> Program name will go here </h5>
                            <?php foreach ($listExercise as $exercise ) {?>
                            <div class="divider"></div>
                            <div class="row line">
                                <div class="col l3">
                                    <div class="training-image">
                                        <img src="web/image/avatar/exercice/pic/<?= $exercise["picto_name"] ?>" alt="développé couché" />
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="training_name">
                                        <b><?= $exercise["libelle"] ?></b>
                                    </div>
                                    <div class="serie">
                                        Série : <?= $exercise["nb_serie"] ?>
                                    </div>
                                    <div class="repetition">
                                        Répetition : <?= $exercise["nb_repeat"] ?>
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="description">
                                        <?= $exercise["description"] ?>
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="video">
                                        <video width="320" height="240" controls>
                                            <source src="..." type="video/mp4">
                                            <source src="..." type="video/ogg">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                </div>

                                <div class="col l12 m12 s12">
                                    <div class="group-musc">

                                    </div>
                                </div>
                            </div>

                            <div class="divider"></div>
                            <?php } ?>

                            <!--<div class="row line">
                                <div class="col l3">
                                    <div class="training-image">
                                        <img src="web/image/legg.png" alt="développé couché" />
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="training_name">
                                        <b>Nom de l'exercice</b>
                                    </div>
                                    <div class="serie">
                                        Série : 4
                                    </div>
                                    <div class="repetition">
                                        Répetition : 12 10 8 6
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Ut tincidunt est a luctus elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Ut tincidunt est a luctus elementum.
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="video">
                                        <video width="320" height="240" controls>
                                            <source src="..." type="video/mp4">
                                            <source src="..." type="video/ogg">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                </div>

                                <div class="col l12 m12 s12">
                                    <div class="group-musc">

                                    </div>
                                </div>
                            </div>

                            <div class="divider"></div>

                            <div class="row line">
                                <div class="col l3">
                                    <div class="training-image">
                                        <img src="web/image/dips.png" alt="développé couché" />
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="training_name">
                                        <b>Nom de l'exercice</b>
                                    </div>
                                    <div class="serie">
                                        Série : 4
                                    </div>
                                    <div class="repetition">
                                        Répetition : 12 10 8 6
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Ut tincidunt est a luctus elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Ut tincidunt est a luctus elementum.
                                    </div>
                                </div>
                                <div class="col l3">
                                    <div class="video">
                                        <video width="320" height="240" controls>
                                            <source src="..." type="video/mp4">
                                            <source src="..." type="video/ogg">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                </div>

                                <div class="col l12 m12 s12">
                                    <div class="group-musc">

                                    </div>
                                </div>
                            </div>

                            <div class="divider"></div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>