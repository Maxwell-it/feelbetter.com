<?php

namespace app\frontend\Module\program;
use AMFram\BackController;

/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 10/05/2016
 * Time: 16:12
 */
class programController extends BackController
{
    public function executeProgram()
    {
        $manager = $this->managers->getManagerOf("program");
        
        $id_user = $this->app->user()->getAttribute("id");
        $listProgram = $manager->getListProgram($id_user);
        $listExercise = $manager->getListExercise();
        
        $this->page->addVar('listProgram', $listProgram);
        $this->page->addVar('listExercise', $listExercise);
        
    }
}