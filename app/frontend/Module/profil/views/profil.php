<div class="row">
    <div class="col l2 m2 s6 sidebar">
        <?php include ('/../../../Templates/sidebar.php');?>
    </div>

    <div class="col l10 m10 s6">

        <div class="middle">
            <nav>
                <div class="nav-wrapper">
                    <div class="col s12">
                        <a href="#!" class="breadcrumb"></a>
                        <a href="profil" class="breadcrumb">Compléter mon profil warrior </a>
                        <?php if ($user->hasFlash()) echo "<p style='color:green' >". $user->getFlash() ."</p>" ?>
                    </div>
                </div>
            </nav>
            <div class="profile-container-properties row">
                <div class="col l4 m4 s12">
                    <div class="profile-setup">
                        <h5> </h5>
                        <div class="profile-pic">
                                <img src="web/image/avatar/profilePicture/<?= $profile["avatar"]?>" class="circle">
                        </div>
                        <div class="pseudo">
                            <h6><?= $profile["username"] ?></h6>
                        </div>
                        <div class="bio">
                            <p><?= $profile["bio"]?></p>
                        </div>
                        <div class="exp">
                            <p><?=$profile["exp"]?> EXP</p>
                        </div>
                        <div>
                            <a class="btn waves-effect modal-trigger" href="#setupProfileForm">Modifier mon profile</a>
                        </div>

                        <div id="setupProfileForm" class="modal">
                            <form method="post" action="" enctype="multipart/form-data">
                                <div class="modal-content">
                                    <input type="hidden" name="change">
                                    <div class="file-field input-field col l12">
                                        <div class="btn">
                                            <span><i class="fa fa-user" aria-hidden="true"></i> </span>
                                            <input type="file" name="profilePic" multiple>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Télécharger votre photo de profil">
                                        </div>
                                    </div>
                                    <div class="input-field col l12">
                                        <textarea id="bio" name="bio" class="materialize-textarea"><?= $profile["bio"] ?></textarea>
                                        <label for="bio">Modifier votre objectif</label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Enregistrer</button>
                                    <button type="reset" href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Annuler</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>