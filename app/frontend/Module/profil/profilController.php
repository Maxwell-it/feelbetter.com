<?php
namespace app\frontend\Module \profil;

use AMFram\BackController;
use AMFram\HTTPRequest;
use Entity\userEntity;

/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 01/08/2016
 * Time: 13:52
 */

class profilController extends BackController
{
    public function executeProfil(HTTPRequest $request)
    {
        $profileMannager = $this->managers->getManagerOf("profil");
        $id_user = $request->getData("id_user");
        $this->app->user()->setAttribute("id",$id_user);
        $profile = $profileMannager->showProfile($id_user);
        $this->page->addVar("profile",$profile);
        
        if($request->postExists("change"))
        {
            $bio = htmlspecialchars($request->postData("bio"));
            $profilePicName = basename($_FILES["profilePic"]["name"]);
            $profilePicSize = $_FILES["profilePic"]["size"];
            $profilePicExtension = strtolower(  substr(  strrchr($profilePicName, '.')  ,1)  );
            $extensions = array("gif","png","jpg","jpeg");
            $uploadDir = __DIR__."/../../../../web/image/avatar/profilePicture/";
            $generatedImgId = md5(uniqid("",true));

            $profilePic = "avatar-".$generatedImgId.".".$profilePicExtension;
            if(!in_array($profilePicExtension, $extensions))
            {
                $this->app->user()->setFlash("Echec lors téléchargement de la photo de profil. Votre photo doit être de type  jpg, jpeg gif ou png");
            }
            else if($profilePicSize > 5000000)
            {
                $this->app->user()->setFlash("La taille de votre photo ne doit pas dépasser 5Mo");
            }
            else
            {
                $user = new userEntity([
                    "id"=>$id_user,
                    "avatar"=>$profilePic,
                    "bio"=>$bio
                ]);
                
                move_uploaded_file($_FILES["profilePic"]["tmp_name"],$uploadDir.$profilePic);
                $profileMannager->updateProfile($user);
                $this->app->user()->setFlash("Votre profile à été modifié avec succès ");
                $this->app->httpResponse()->redirection("profil-".$id_user);
            }
        }

    }
}