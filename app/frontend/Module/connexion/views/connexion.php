<!--<div class="connexion">
    <div class="container ">
        <div class="bg-connexion-img"></div>
        <div class="connexion-wrapper row">
            <div class="col l6 m6 s12">
                <h5 data-in-effect="fadeIn" class="titre">
                    Un large choix de programmes de sport proposés par toute une communauté
                </h5>
                <br><br><br>
                <h5 data-in-effect="fadeIn" class="titre2">
                    Vos amis et les sportifs du monde entier vous accompagnent dans votre motivation...
                </h5>
            </div>
            <div class="wrapper col s12 m6 l6">
                <h4> Connexion</h4>

                <form action="" method="post">
                    <div class="row">
                        <div class="input-field col l10 m10 s10">
                            <input id="pseudo" type="text" name="pseudo" class="validate" autocomplete="off">
                            <label for="pseudo">Pseudo</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col l10 m10 s10">
                            <input id="password" type="password" name="password" class="validate" autocomplete="off">
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <button class="btn" type="submit">Send</button>
                </form>
                <a href="register" class="waves-effect waves-light btn-flat inscription"> Inscription </a>
            </div>
        </div>
    </div>



</div>-->

<div class="connexion-wrapper">
    <div class="wrapper grey darken-4">
        <form action="" method="post">
            <div class="image-container blue-grey darken-2 row">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <b style="font-size: 18px; margin-left: 10px"> Connexion</b>
            </div>
            <?php if ($user->hasFlash()) echo "<p style='color:orangered' >". $user->getFlash() ."</p>" ?>
            <div class="row">
                <div class="input-field col l12 m12 s12">
                    <input id="pseudo" type="text" name="pseudo" class="validate" autocomplete="off">
                    <label for="pseudo">Pseudo</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col l12 m12 s12">
                    <input id="password" type="password" name="password" class="validate" autocomplete="off">
                    <label for="password">Password</label>
                </div>
            </div>

            <div class="row">
                <div class="col l12">
                    <button class="btn col l12" type="submit">se connecter</button>
                </div>
            </div>

            <div class="row">
                <a href="register" class="waves-effect btn-flat btn-forgot-pass col l6"> Mot de passe oublié </a>
                <div class="col l6">
                    <a href="register" class="waves-effect btn-flat btn-register"> S'inscrire</a>
                </div>
            </div>
        </form>
    </div>
</div>