<?php
namespace app\frontend\Module\connexion;
use AMFram\BackController;
use AMFram\HttpRequest;
use Entity\userEntity;

/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 24/04/2016
 * Time: 22:54
 */
class connexionController extends BackController
{
    public function executeConnexion(HTTPRequest $request)
    {
        if($request->postExists("pseudo") && $request->postExists("password"))
        {
            $manager = $this->managers->getManagerOf("connexion");

            $username = htmlspecialchars($request->postData('pseudo'));
            //$password = md5(htmlspecialchars($request->postData('password')));
            $password = htmlspecialchars($request->postData('password'));

            $user = new userEntity([
                'pseudo'=>$username,
                'password'=>$password
            ]);

            if ($manager->getUser($user))
            {
                $user_infos = $manager->getUser($user);
                $id_user = $user_infos["id_user"];
                $this->app->user()->setAuthenticated(true);
                $this->app->user()->setAttribute("id",$id_user);
                $this->app->httpResponse()->redirection('index');
                $this->app->user()->setFlash("Vous ête maintenant connecté");
            }
            else
            {
                $this->app->user()->setFlash('Identifiants incorrect. Veillez rééssayer à nouveau');
            }
        }
    }
}