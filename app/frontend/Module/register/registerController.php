<?php

namespace app\frontend\Module\register;

use AMFram\BackController;
use AMFram\HTTPRequest;
use Entity\userEntity;

/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 02/05/2016
 * Time: 12:46
 */
class registerController extends BackController
{
    public function executeRegister( HTTPRequest $request)
    {
        $manager = $this->managers->getManagerOf("register");
        $username = htmlspecialchars($request->postData("pseudo"));
        $email = md5(htmlspecialchars($request->postData("email")));
        $password = md5(htmlspecialchars($request->postData("password")));
        $password_confirm = md5(htmlspecialchars($request->postData("password_confirm")));

        if($request->postExists("pseudo"))
        {
            if($password == $password_confirm)
            {
                $user = new userEntity([
                    'pseudo'=>$username,
                    'password'=>$password,
                    'email'=>$email
                ]);
                
                if($user->is_valid())
                {
                    $manager->save($user);
                    $id_user = $manager->getLastInsertId();
                    $this->page->addVar("id_user",$id_user);
                    $this->app->user()->setFlash('Votre compte a été bien créé !');
                    $this->app->user()->setAuthenticated(true);
                    $this->app->httpResponse()->redirection('profil-'.$id_user);
                }
            }
            else
            {
                $this->app->user()->setFlash('Les mots de passe ne correspondent pas');
            }
        }
    }
}