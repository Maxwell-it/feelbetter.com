<div class="register-wrapper">
    <div class="wrapper grey darken-4">
        <form action="" method="post">
            <input type="hidden" name="id_user" value="<?=$id_user?>">
            <div class="image-container blue-grey darken-2 row">
                <i class="fa fa-user" aria-hidden="true"></i>
                <b style="font-size: 18px; margin-left: 10px"> Créer un compte</b>
            </div>
            <div class="row">
                <div class="input-field col l12">
                    <input id="pseudo" type="text" name="pseudo" class="validate">
                    <label for="pseudo">Pseudo</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col l12">
                    <input id="email" type="email" name="email" class="validate">
                    <label for="email">Email </label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col l6">
                    <input id="password" type="password" name="password" class="validate">
                    <label for="password">Password</label>
                </div>
                <div class="input-field col l6">
                    <input id="password" type="password" name="password_confirm" class="validate">
                    <label for="password">Confirmation password</label>
                </div>
            </div>

            <div class="row">
                <div class="col l12">
                    <button class="btn col l12" type="submit">Créer mon compte</button>
                </div>
            </div>
        </form>
    </div>
</div> 