<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 02/06/2016
 * Time: 19:28
 */

namespace app\frontend\Module\addProgram;


use AMFram\BackController;
use AMFram\HTTPRequest;
use Entity\Exercice;
use Entity\Programs;
use Symfony\Component\Finder\SplFileInfo;

class addProgramController extends BackController
{
    public function executeAddProgram(HTTPRequest $request)
    {

        // ajout du programme
        if($request->postExists("name_program"))
        {
            $manager = $this->managers->getManagerOf('addProgram');
            $name_program = $request->postData("name_program");
            $description = $request->postData("description");
            $id_user = $this->app->user()->getAttribute("id");

            $program = new Programs([
                'name_program'=>$name_program,
                'description'=>$description
            ]);

            if($program->is_valid())
            {
                $manager->addProgram($program);
                $id_program = $manager->getLastInsertId();
                $manager->bindUserToProg($id_user,$id_program);
                $this->page->addVar('id_program',$id_program);
                $this->app->httpResponse()->redirection("add_exercice-".$id_program);
            }
        }

    }

    public  function executeAddExercice(HTTPRequest $request)
    {

        // get manager of addProgram
        $getProgramManager = $this->managers->getManagerOf("addProgram");
        // get exercise list and send variable
        $listMuscExercice = $getProgramManager->exoMuscSelect();
        $this->page->addVar('listMuscExercice',$listMuscExercice);
        $listCardioExercice = $getProgramManager->exoCardioSelect();
        $this->page->addVar('listCardioExercice',$listCardioExercice);

        if($request->postExists("exo"))
        {

            $exo = $request->postData("exo");
            $serie = $request->postData("serie");
            $rep = $request->postData("rep");
            $description = $request->postData("description");
            $repos = $request->postData("repos");
            $exo_pic = $request->postData("exo_pic");
            $exo_vid = $request->postData("exo_vid");


            // move upload file
            $extensions = array( 'jpg' , 'jpeg' , 'gif' , 'png', 'bmp' );
            $pic = basename($_FILES["exo_pic"]["name"]);
            $pic_size = $_FILES["exo_pic"]["size"];
            $pic_extension = strtolower(  substr(  strrchr($pic, '.')  ,1)  );
            $path_pic = __DIR__."/../../../../web/image/avatar/exercice/pic/";
            $generatedImgId = md5(uniqid("",true));
            $pic_name = "exo-pic-".$generatedImgId.".".$pic_extension;

            // check if extension is valid
            if (!(in_array($pic_extension,$extensions) ))
            {
                echo "Votre image doit être de type  jpg, jpeg gif ou png ";
            }
            else if ($pic_size > 5000000)
            {
                echo "Le téléchargement ne dois pas dépasser 5 MO";
            }
            else
            {
                $upload_pic = move_uploaded_file($_FILES["exo_pic"]["tmp_name"],$path_pic.$pic_name);
                if($upload_pic)
                {
                    echo "succes: so do something like show img or descript what is uploaded : ".$pic_name;
                }
            }

            $exercice = new Exercice([
                "idlistexo"=>$exo,
                "serie"=>$serie,
                "rep"=>$rep,
                "description"=>$description,
                "repos"=>$repos,
                "exoPic"=>$pic_name
            ]);

            $getProgramManager->addExercice($exercice);
            $idExo = $getProgramManager->getLastInsertId();
            $idProgram = $request->getData("id_program");
            $getProgramManager->bindExoToProg($idProgram, $idExo);
        }

    }

}