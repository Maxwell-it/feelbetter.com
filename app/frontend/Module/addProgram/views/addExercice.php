<div class="row">
    <div class="col l2 m2 s6 sidebar">
        <?php include ('/../../../Templates/sidebar.php');?>
    </div>

    <div class="col l10 m6 s6">
        <div class="middle">
            <div class="addprog">
                <form action="" method="post" class="form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col l6">
                            <h5>Ajouter un exercice au programme</h5>
                            <div class="input-field col s12">
                                <select name="exo">
                                    <optgroup label="">
                                        <option value="0" selected disabled>Choisir un exercice </option>
                                    </optgroup>
                                    <optgroup label="Musculation">
                                        <?php foreach ($listMuscExercice as $exerciceMusc){?>
                                            <option value="<?= $exerciceMusc['id_list_exo'] ?>"><?= $exerciceMusc["libelle"] ?></option>
                                        <?php }?>
                                    </optgroup>
                                    <optgroup label="Cardio">
                                        <?php foreach ($listCardioExercice as $exerciceCardio){?>
                                            <option value="<?=$exerciceCardio['id_list_exo'] ?>"><?= $exerciceCardio["libelle"] ?></option>
                                        <?php }?>
                                    </optgroup>
                                </select>
                                <label>Choisir un exercice </label>
                            </div>


                            <div class="input-field col l4">
                                <select name="serie">
                                    <?php for ($serie = 1; $serie<= 20; $serie++){?>
                                        <option value="<?=$serie?>"><?= $serie ?></option>
                                    <?php }?>
                                </select>
                                <label>Nombre de serie</label>
                            </div>
                            <div class="input-field col l4">
                                <select name="rep">
                                    <?php for ($rep = 1; $rep<= 20; $rep++){?>
                                        <option value="<?=$rep?>"><?= $rep ?></option>
                                    <?php }?>
                                </select>
                                <label>Nombre de répétiton</label>
                            </div>
                            <div class="input-field col l4">
                                <select name="repos">
                                    <?php for ($repos = 1; $repos<= 20; $repos++){?>
                                        <option value="<?=$repos?>"><?= $repos ?></option>
                                    <?php }?>
                                </select>
                                <label>Temps de repos</label>
                            </div>


                            <div class="input-field col l12">
                                <textarea id="exo-desc" name="description" class="materialize-textarea"></textarea>
                                <label for="exo-desc">Une petite description de l'exercice ?</label>
                            </div>

                            <div class="file-field input-field col l8">
                                <div class="btn">
                                    <span>Photo</span>
                                    <input type="file" name="exo_pic" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Joindre une image a l'exercice">
                                </div>
                            </div>
                            <!--<div class="file-field input-field col l12">
                                <div class="btn">
                                    <span>video</span>
                                    <input type="file" name="exo_vid" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Ajouter une video pour décrire votre exercice">
                                </div>
                            </div>-->
                           <div class="col l4">
                              <button class="btn waves-effect modal-trigger red" data-target="modal-video" href="#modal-video" style="position:relative; top:18px; height: 42px; width: 100%"><i class="fa fa-play" aria-hidden="true"></i> Vidéo</button>
                           </div>

                            <div class="col l12">
                                <button style="float: left; width: 45%;" class="btn waves-effect waves-light" type="submit">Valider</button>
                                <button style="float: right; width: 45%;" class="btn waves-effect waves-light" type="reset">Reinitialiser</button>
                            </div>

                            <!-- modal exercise -->
                            <div id="modal-exo" class="modal">
                                <div class="modal-content">
                                    <h5>Votre exercice a été bien ajouté</h5>
                                    <p>Ajouter d'autres exercices pour finaliser la création de votre programme</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn add_exercice">
                                        Ajouter un autre exercice
                                    </a>
                                    <a href="#!" class="modal-action modal-close waves-effect waves-light btn-flat ignore">
                                        Continuer
                                    </a>
                                </div>
                            </div>

                            <!-- modal video --->
                            <div id="modal-video" class="modal">
                                <div class="modal-content">
                                    <a class="btn-floating btn-large red">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                    <a class="btn-floating btn-large">
                                        <img src="web/image/logo/Dlm.jpg" width="52px">
                                    </a>
                                    <a class="btn btn-floating btn-large">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="max_file_size" value="5000000" />

                </form>
            </div>
        </div>
    </div>
</div>