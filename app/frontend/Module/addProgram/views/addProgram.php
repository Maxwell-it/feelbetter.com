<div class="row">
    <div class="col l2 m2 s6 sidebar">
        <?php include ('/../../../Templates/sidebar.php');?>
    </div>

    <div class="col l10 m6 s6">
        <div class="middle">
            <div class="addprog">
                <form action="" method="post" class="form">
                    <div class="row">
                        <div class="align col l6">
                            <div class="row progtitle">
                                <h5>Créer un programme </h5>
                                <div class="input-field col l12">
                                    <input id="name_program" type="text" name="name_program" class="validate">
                                    <label for="name_program"> Nom du programme </label>
                                    <span class="error">Veillez au moins remplir ce champs pour créer un programme</span>
                                </div>

                                <div class="input-field col l12">
                                    <textarea id="exo-desc" name="description" class="materialize-textarea"></textarea>
                                    <label for="exo-desc">Une petite description du programme ?</label>
                                </div>

                                <div class="col l12">
                                    <button style="float: right;" class="btn waves-effect waves-light program_name_submit" type="submit" >Valider</button>
                                </div>

                                <!-- modal-->
                                <div id="modal-prog" class="modal">
                                    <div class="modal-content">
                                        <h5 class="succes-msg">Votre programme a été créé</h5>
                                        <p>Ajouter des exercices pour finaliser la création de votre programme</p>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id_program" value="1">
                                        <a class="modal-action modal-close waves-effect waves-green btn add_exercice">
                                            Ajouter un exercice
                                        </a>
                                        <a href="#!" class="modal-action modal-close waves-effect waves-light btn-flat ignore">
                                            plus tard
                                        </a>
                                    </div>
                                </div>
                                
                                
                            </div>

                        </div>
                    </div>
                    <input type="hidden" name="id_program" value="<?= $id_program ?>">
                </form>
            </div>
        </div>
    </div>
</div>