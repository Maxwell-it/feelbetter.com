<div class="row">

    <div class="sidebar-position sidebar">
        <?php include ('/../../../Templates/sidebar.php');?>
    </div>

    <!--tableau de bord --->
    <div class="content-position">
        <div class="middle">
            <nav>
                <div class="nav-wrapper">
                    <div class="col s12">
                        <a href="#!" class="breadcrumb"></a>
                        <a href="index" class="breadcrumb">dashboard</a>
                        <?php if ($user->hasFlash()) echo "<p style='color:green' >". $user->getFlash() ."</p>" ?>
                    </div>
                </div>
            </nav>

            <div class="dash">
                <div class="programs">

                    <div class="content">
                        <div class="row">
                            <div class="col l8 m8 s12 list-prog">
                                <div class="title">
                                    <h5> Mes programmes </h5>
                                </div>
                                <ul class="collapsible white" data-collapsible="accordion">
                                    <?php foreach ($listProgramIndex as $program){?>
                                    <li>
                                        <div class="collapsible-header">
                                            <div class="row">
                                                <div class="col l1"><span class="number">0<?= $program['id_prog_creer']?> </span></div>
                                                <div class="col l6"><?= $program['nom_prog']?></div>
                                                <div class="col l3">
                                                    <div class="chip">
                                                        <img src="web/image/user-f.png" alt="Contact Person">
                                                        Jane Doe
                                                    </div>
                                                </div>
                                                <div class="col l2"><span class="new badge">9999+</span></div>
                                            </div>
                                        </div>
                                        <div class="collapsible-body">
                                            <div class="row">
                                                <div class="col l10 m10 s10">
                                                    <p> <?= $program['description'] ?><br >
                                                        <a class="waves-effect waves-light btn btn-voir">voir</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="">
                                    <a href="program" class="waves-effect waves-light btn"> <i class="fa fa-tasks" aria-hidden="true"></i> Voir tous les programmes </a>
                                </div>
                            </div>
                            <div class="col l4 m4 s12">
                                <div class="title">
                                    <h5>Séance à venir</h5>
                                </div>

                                <div class="row">
                                    <div class="col l3 m3 s12">
                                        <p> <b> Lundi : <br> </b><span class="date-rapel"> 07/05/2016</span>  </p>
                                    </div>
                                    <div class="col l9 m9 s12">
                                        <p><b>Nom du programme</b> </p>
                                        <div class="divider"></div>
                                        <p class="description"> Description du programme - Description du programme - Description du programme. <br>
                                            <a class="waves-effect waves-light btn btn-voir">voir</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="flux">

                
                <div class="row">
                    <div class="col l6 m6 s12">
                        <!-----------------------------------------formulaire de publication ------------------------>
                        <form class="share-form z-depth-1" action="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="input-field col l12">
                                    <i class="material-icons prefix">mode_edit</i>
                                    <textarea id="publier" class="materialize-textarea" name="post_content"></textarea>
                                    <label for="publier">Publier vos programmes, vos performances ou vos astuces</label>
                                </div>
                                <div class="col l1"><br></div>
                                <div class="col l5">
                                    <button class="btn waves-effect">
                                        <i class="fa fa-share" aria-hidden="true"></i>
                                        programmes
                                    </button>
                                </div>
                                <div class="btn col l2 file-field">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                    <input type="file" name="post_media">
                                </div>
                                <div class="col l2">
                                    <button class="btn waves-effect waves-light" type="submit" name="action">publier</button>
                                </div>
                            </div>
                        </form>
                        
                        
                        <!-- post 1-->
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card">
                                    <!--image-->
                                    <div class="card-image">
                                        <img src="web/image/footing.jpg">
                                        <span class="card-title">Image Title</span>
                                    </div>
                                    <!--content-->
                                    <div class="card-content">
                                        <div class="row">
                                            <div class="col l2 m2 s2">
                                                <img src="web/image/user.png" alt="" class="circle responsive-img">
                                            </div>
                                            <div class="col l10 m10 s10">
                                                <p><span class="username">John doe</span> <br> <span class="date">08/05/2016</span></p>
                                                <p>I am a very simple card. I am good at containing small bits of information.
                                                    I am convenient because I require little markup to use effectively.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-action">
                                        <span class="new badge">9999+</span>
                                        <a href="#">Partager</a>
                                    </div>
                                    <div class="divider"></div>
                                    <!-- comment -->
                                    <div class="row comment">
                                        <div class="col l2 m2 s2 cmt-img-div">
                                            <img src="web/image/user-f.png" alt="" class="circle responsive-img img-c">
                                        </div>
                                        <div class="col l10 m10 s10">
                                            <div class="card-content">
                                                <p>I am a very simple comment. I am good at containing small bits of information.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row comment">
                                        <div class="col l2 m2 s2 cmt-img-div">
                                            <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                        </div>
                                        <div class="col l9 m9 s9">
                                            <form>
                                                <div class="input-field ">
                                                    <input id="comment" type="text" class="validate">
                                                    <label for="comment">Votre commentaire</label>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- post 2-->
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="row">
                                            <div class="col l2 m2 s2">
                                                <img src="web/image/user.png" alt="" class="circle responsive-img">
                                            </div>
                                            <div class="col l10 m10 s10">
                                                <p><span class="username">John doe</span> <br> <span class="date">08/05/2016</span></p>
                                                <p>I am a very simple card. I am good at containing small bits of information.
                                                    I am convenient because I require little markup to use effectively.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-action">
                                        <span class="new badge">9999+</span>
                                        <a href="#">Partager</a>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="row comment">
                                        <div class="col l2 m2 s2 cmt-img-div">
                                            <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                        </div>
                                        <div class="col l9 m9 s9">
                                            <form>
                                                <div class="input-field ">
                                                    <input id="comment" type="text" class="validate">
                                                    <label for="comment">Votre commentaire</label>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                   <!-- <div class="col l4 m4 s12">
                        <div class="row best-program-user">
                            <ul class="collection">
                                <li class="collection-item active"> Meilleurs programmes</li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p><b> Nom du programme </b> <br>
                                        100 000 votes
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p><b> Nom du programme </b>  <br>
                                        10 000 votes
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p><b> Nom du programme </b> <br>
                                        1000 votes
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p><b> Nom du programme </b><br>
                                        100 votes 
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p><b> Nom du programme </b> <br>
                                        10 votes
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
                                </li>
                            </ul>
                        </div>
                    </div>-->

                    <!--<div class="col l3 m3 s12">
                        <div class="row user-online ">
                            <ul class="collection">
                                <li class="collection-item active"> En ligne</li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p> 120120 EXP <br>
                                        <span class="online">En ligne</span>
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="fa fa-circle" aria-hidden="true"></i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p><p> 1200 EXP <br>
                                        <span class="online">En ligne</span>
                                    </p>
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="fa fa-circle" aria-hidden="true"></i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p> 1320 EXP <br>
                                        <span class="online">En ligne</span>
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="fa fa-circle" aria-hidden="true"></i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p> 3020 EXP <br>
                                        <span class="online">En ligne</span>
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="fa fa-circle" aria-hidden="true"></i></a>
                                </li>
                                <li class="collection-item avatar">
                                    <img src="web/image/user.png" alt="" class="circle responsive-img img-c">
                                    <span class="username">John doe</span>
                                    <p> 55100 EXP <br>
                                        <span class="online">En ligne</span>
                                    </p>
                                    <a href="#!" class="secondary-content"><i class="fa fa-circle" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>-->
                </div>

            </div>
        </div>
    </div>

</div>