<?php
namespace app\frontend\Module\dashboard;
use AMFram\BackController;
use AMFram\HTTPRequest;
use Entity\PostEntity;

/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 07/04/2016
 * Time: 00:15
 */
class dashboardController extends BackController
{
    public function executeDashboard(HTTPRequest $request)
    {
        $manager = $this->managers->getManagerOf("dashboard");

        $id_user = $this->app->user()->getAttribute("id");

        /***************** programme list *********************/
        $listProgramIndex = $manager->showIndexProgram($id_user);
        $this->page->addVar('listProgramIndex', $listProgramIndex);

        /************ profile user ****************/
        $profile = $manager->showProfile($id_user);
        $this->page->addVar("profile",$profile);

        /*************** show post ****************/

        /*************    post   **********************/
        if($request->postExists("post_content"))
        {
            $content = $request->postData("post_content");
            $postMediaName = basename($_FILES["post_media"]["name"]);
            $postMediaNameExtension = strtolower( substr( strrchr($postMediaName,"."),1 ) );

            $extensionPicto = array("jpg", "jpeg", "png", "gif") ;
            $extensionVideo = array("mov", "flv","mpeg4" ,"mp4", "webm", "avi", "wmv");
            $pictoDir = __DIR__."/../../../../web/post/image/";
            $videoDir = __DIR__."/../../../../web/post/video/";
            $generatedPictoName = md5(uniqid("",true));
            $generatedVideoName = md5(uniqid("",true));
            $pictoName = "";
            $videoName = "";


            if(in_array($postMediaNameExtension , $extensionPicto))
            {
                $pictoName = "postImage-".$generatedPictoName.".".$postMediaNameExtension;
                move_uploaded_file($_FILES["post_media"]["tmp_name"],$pictoDir.$pictoName);
                $this->app->user()->setFlash("image déplacée");
                $videoName = "";
            }
            else if (in_array($postMediaNameExtension , $extensionVideo))
            {
                $videoName = "postVideo-".$generatedVideoName.".".$postMediaNameExtension;
                move_uploaded_file($_FILES["post_media"]["tmp_name"],$videoDir.$videoName);
                $this->app->user()->setFlash("video dépacée");
                $pictoName = "";
            }
            else
            {
                $this->app->user()->setFlash("Erreur lors téléchargement: Votre devez télécharger des image de type jpg, jpeg, png gif et ou des videos de type mp4, avi ou webm ");
            }

            $post = new PostEntity([
                "content"=>$content,
                "picture"=>$pictoName,
                "video"=>$videoName
            ]);

            if ($post->is_valid())
            {
                $manager->addPost($post);
                $id_post = $manager->getLastInsertId();
                $manager->bindUserToPost($id_user, $id_post);
                $this->app->user()->setFlash("votre publication à été bien enregistré");
                $this->app->httpResponse()->redirection("index");
            }
            else
            {
                $this->app->user()->setFlash("Vous n'avez rien publier");
            }
        }


    }

    public function executeInsertComment(HTTPRequest $request)
    {
        $this->page->addVar('title', 'ajout commentaire');

        if ($request->postExists('comment'))
        {
            $comment = new Comment([
                'user' =>$request-getDate('user'),
                'post' =>$request-getDate('post'),
                'content' =>$request-postDate('content'),
            ]);
        }
        
        if ($comment->is_valid())
        {
            $this->managers->getManagerOf('comment')->save($comment);
            
            $this->app->user()-setFlash('votre commentaire a été envoyé');
            
            $this->app->httpResponse()->redirect('post-' .$request);
        }
        else
        {
            $this->page->addVar('erreur', $comment->erreurs());
        }
        $this->page->addVar('comment','$comment');
    }

    public function executeCommentShow(HTTPRequest $request)
    {
        $post = $this->managers->getManagerOf('post')->getUnique($request->getData('id'));

        if (empty($post))
        {
            $this->app->httpreponse()->redirect404();
        }

        $this->page->addVar('title',$post->titre());
        $this->page->addVar('post',$post);
        $this->page->addVar('comment',$this->managers->getManagerOf('comment')->getListOf($post->id()));
        
    }
}