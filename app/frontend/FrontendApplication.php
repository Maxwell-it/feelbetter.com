<?php
/**
 * Created by PhpStorm.
 * User: maxwell
 * Date: 06/04/2016
 * Time: 15:14
 */

namespace app\Frontend;

use AMFram\Application;
use app\frontend\Module\connexion\connexionController;
use app\frontend\Module\register\registerController;


class frontendApplication extends Application
{
    public function __construct()
    {
        parent::__construct();
        
        $this->name = "frontend";
    }

    public function run()
    {
       /* if($this->user->isAuthenticated())
        {
            $controller = $this->getController();
        }
        else
        {
            if ($this->httpRequest->requestURI() == "/dashboard_fitness/register")
            {
                $controller = new registerController($this, "register", "register");
            }
            else
            {
                $controller = new connexionController($this, "connexion", "connexion");
            }
        }*/

        $controller = $this->getController();
        $controller->execute();
        $this->httpResponse->setPage($controller->page());
        $this->httpResponse->send();
       
    }
}