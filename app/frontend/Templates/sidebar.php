<div class="bg-image-sidebar"></div>

<!-- user profil-->
<div class="row profil">

    <div class="profil-pic">
        <img src="web/image/avatar/profilePicture/<?= $profile["avatar"] ?>" class="circle">
    </div>

    <div class="pseudo">
        <h6><?= $profile["username"] ?></h6>
    </div>

    <div class="bio">
        <p><?= $profile["bio"] ?></p>
    </div>
    
    <div class="exp">
        <p><?=$profile["exp"]?> EXP</p>
    </div>
</div>

<div class="menu">
    <nav>
        <ul class="right hide-on-med-and-down">
            <li><a href="index"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
            <li><a href="program"><i class="fa fa-tasks" aria-hidden="true"></i> Programs</a></li>
            <li><a href="#!"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Publications</a></li>
            <li><a href="#!"><i class="fa fa-users" aria-hidden="true"></i> Friends</a></li>
            <li><a href="#!"><i class="fa fa-exclamation" aria-hidden="true"></i> About</a></li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
    </nav>
</div>