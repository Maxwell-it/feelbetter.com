<!DOCTYPE html>
<html lang="en">
  <head>
	  <!-- Required meta tags always come first -->
	  <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <meta http-equiv="x-ua-compatible" content="ie=edge">
	  <!-- Materialize-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
      <link rel="stylesheet" href="http://localhost/dashboard_fitness/web/css/materialize/css/materialize.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	  <link rel="stylesheet" href="http://localhost/dashboard_fitness/web/css/font-awesome/font-awesome/css/font-awesome.min.css">
	  <link rel="stylesheet" href="http://localhost/dashboard_fitness/web/vendor/fullcalendar-2.6.1/fullcalendar-2.6.1/fullcalendar.css" >
	  <!-- My StyleSheet -->
	  <link rel="stylesheet" href="/dashboard_fitness/web/css/style.css">
	  <link rel="stylesheet" href="/dashboard_fitness/web/css/connexion.css">
	  <link rel="stylesheet" href="/dashboard_fitness/web/css/materialize/css/materialize.min.css">
      <link rel="stylesheet" href="/dashboard_fitness/web/vendor/textillate-master/assets/animate.css">
      <title>FeelBetter</title>
  </head>
  <body class="main">
	<div class="">
        <div class="navbar-fixed">
            <nav>
                <div class="theme nav-wrapper">
                    <a href="index" class="brand-logo"> Warriors </a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="add" class="blue-grey darken-1">Créer un programme</a></li>
                        <li><a href="#"><i class="material-icons">search</i></a></li>
                        <li><a href="#"><i class="material-icons">view_module</i></a></li>
                        <li><a href="deconnexion"><i class="material-icons">power_settings_new</i></a></li>
                        <li><a href="#"><i class="material-icons">more_vert</i></a></li>
                    </ul>
                </div>
            </nav>
        </div>
            <?= $content ?>
	</div>
    <!-- jQuery first, then Bootstrap JS. -->
	<script src="http://localhost/dashboard_fitness/web/js/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	<script src="/dashboard_fitness/web/css/materialize/js/materialize.min.js"></script>
	<script src="http://localhost/dashboard_fitness/web/js/animate.js"></script>
	<script src="http://localhost/dashboard_fitness/web/js/function.js"></script>
	<script src="http://localhost/dashboard_fitness/web/js/calendar.js"></script>
	<script src="http://localhost/dashboard_fitness/web/vendor/fullcalendar-2.6.1/fullcalendar-2.6.1/lib/moment.min.js"></script>
	<script src="http://localhost/dashboard_fitness/web/vendor/fullcalendar-2.6.1/fullcalendar-2.6.1/fullcalendar.js"></script>
    <script src="http://localhost/dashboard_fitness/web/vendor/flot-flot-v0.8.3-0-g453b017/flot-flot-453b017/jquery.flot.js"></script>
    <script src="http://localhost/dashboard_fitness/web/js/chart.js"></script>
    <script src="http://localhost/dashboard_fitness/web/vendor/textillate-master/assets/jquery.fittext.js"></script>
    <script src="http://localhost/dashboard_fitness/web/vendor/textillate-master/jquery.textillate.js"></script>
    <script src="http://localhost/dashboard_fitness/web/vendor/textillate-master/assets/jquery.lettering.js"></script>

  </body>
</html>